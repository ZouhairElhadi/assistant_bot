pipeline {
    agent any

    options {
    buildDiscarder(logRotator(numToKeepStr: '10'))
    timestamps() 
    timeout(time: 20, unit: 'MINUTES')
  }

    environment {
        projectName = 'masterbot'
        VIRTUAL_ENV = "${env.WORKSPACE}/venv"
    }

    stages {
        stage('Checkout') {
            steps {
                  echo '********* Checkout Stage Started **********'

                  checkout scm

                  echo '********* Checkout Stage Finished **********'
            }
        }
        stage('Environnement Preperation') {
            steps {
                    echo '********* Preperation Stage Started **********'
                    sh "rm -rf venv"
                    sh "virtualenv venv"
                    sh "virtualenv -p /usr/bin/python3.7 venv"
                    sh "${VIRTUAL_ENV}/bin/python3.7 -m pip install --upgrade pip"
                    echo '********* Preperation Stage Finished **********'
            }
        }
        stage('Build') {
            steps {
                echo '********* Build Stage Started **********'
                sh "source ${VIRTUAL_ENV}/bin/activate && ${VIRTUAL_ENV}/bin/pip3 install --no-cache-dir -r requirements.txt"
                echo '********* Build Stage Finished **********'
            }
        }
        stage('Static code metrics') {
            steps {
                echo '********* Static Analysis Stage Started **********'

                echo "Raw metrics"
                sh  ''' source ${VIRTUAL_ENV}/bin/activate &&
                    radon raw --json Application > raw_report.json &&
                    radon cc --json Application > cc_report.json &&
                    radon mi --json Application > mi_report.json
                '''
                echo "CheckStyle"
                sh  ''' source ${VIRTUAL_ENV}/bin/activate &&
                        pylint --disable=W1202 --output-format=parseable --reports=no Application > pylint.log || echo "pylint exited with $?"
                    '''

                echo '********* Static Analysis Stage Finished **********'
            }
            post{
                always{
                    step([$class: 'WarningsPublisher',
                            parserConfigurations: [[
                            parserName: 'PYLint',
                            pattern: 'pylint.log'
                                      ]],
                            unstableTotalAll: '0',
                            usePreviousBuildAsReference: true])
                }
            }
        }

        stage('Docker Build') {
            steps {
                echo '*********create tag*********'
                sh "docker build . --tag master-chatbot"
                echo '*********create tag*********'
            }
        }
        stage('Clean Up') {
            steps {
                    echo '********* Clean Up Stage Started **********' 
                    sh "rm -rf venv"
                    echo '********* Clean Up Stage Finished **********'
            }
        }
    }
    post {
        success {
           script {
                msg = "Build success for ${env.JOB_NAME} ${env.BUILD_NUMBER} (${env.BUILD_URL})"
                slackSend channel: 'jenkins-job', color: '#439FE0', message: msg, teamDomain: 'zine-it', tokenCredentialId: 'slack-crendentials'
            }
        }
        failure {
            script {
                msg = "Build error for ${env.JOB_NAME} ${env.BUILD_NUMBER} (${env.BUILD_URL})"
               slackSend failOnError: true, channel: 'jenkins-job', message: msg, teamDomain: 'zine-it', tokenCredentialId: 'slack-crendentials'
            }
        }
        unstable {
            script {
                msg = "Build unstable for ${env.JOB_NAME} ${env.BUILD_NUMBER} (${env.BUILD_URL})"
               slackSend failOnError: true, channel: 'jenkins-job', message: msg, teamDomain: 'zine-it', tokenCredentialId: 'slack-crendentials'
            }
        }
        changed {
            script {
                msg = "Pipeline changed for ${env.JOB_NAME} ${env.BUILD_NUMBER} (${env.BUILD_URL})"
               slackSend failOnError: true, channel: 'jenkins-job', message: msg, teamDomain: 'zine-it', tokenCredentialId: 'slack-crendentials'
            }
        }
    }
}