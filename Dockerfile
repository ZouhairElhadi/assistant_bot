FROM python:3.7.9

WORKDIR /masterbot

COPY . /masterbot

RUN /usr/local/bin/python -m pip install --upgrade pip
RUN pip install --upgrade setuptools

RUN pip --no-cache-dir install -r requirements.txt
RUN /usr/local/bin/python -m spacy download en_core_web_md
RUN chmod -R 777 /masterbot

EXPOSE 5005

CMD ["rasa", "run", "--enable-api"]
